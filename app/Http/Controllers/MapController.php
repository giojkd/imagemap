<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Str;
use Storage;
use App\Map;

class MapController extends Controller
{
    //
    public function store(Request $request){

        $image_64 = $request->imageUpload; //your base64 encoded data
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', '+', $image);
        Storage::disk('public')->put($request->imageName, base64_decode($image));

        $map = Map::create([
            'name' => $request->name,
            'image' => 'storage/'.$request->imageName,
            'map' => $request->map
        ]);

        return back();

    }

    public function delete(Request $request)
    {
        $map = Map::findOrFail($request->id);
        $map->delete();
        return back();
    }


}

@extends('layouts.app')

@push('head')




<script>
    var imagePrefix = '{{ env('APP_URL') }}/storage/';
</script>

@endpush

   @section('content')

    <div class="container">
         <div class="row">
            <div class="col-md-12">
                <h2>Mappe salvate</h2>
                @if(!is_null($maps) && $maps->count() > 0)
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Contenuto</th>
                            <th>Azioni</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($maps as $map)
                            <tr>
                                <td>
                                    <img src="/{{ $map->image }}" alt="" style="height: 50px">
                                </td>
                                <td>{{ $map->id }}</td>
                                <td>{{ $map->name }}</td>
                                <td><textarea class="form-control">{{ $map->map }}</textarea></td>
                                <td>
                                    <form action="{{ Route('mapDelete') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $map->id }}">
                                        <button type="submit" class="btn btn-danger">Cancella</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <div class="alert alert-secondary">
                        Non hai creato nessuna mappa ancora
                    </div>
                @endif
            </div>
        </div>
    </div>

      <div class="container mt-4">
         <div class="row">
            <div class="col-md-12">
                <h2>Crea una nuova mappa</h2>
               <div class="step">
                   <button type="button" class="btn btn-success btn-lg" id="image-mapper-upload">Seleziona un'immagine dal mio pc</button>
                   <input type="file" name="" id="image-mapper-file">
                   <!--<span class="divider">
                       &nbsp; &nbsp; &nbsp; -- OR -- &nbsp; &nbsp; &nbsp;
                    </span>
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#image-mapper-load">Load Image from Website</button>-->
                </div>
            </div>
         </div>
      </div>
      <div class="container toggle-content" style="display: none;">
         <div class="row">
            <div class="col-md-12">
               <div class="container">
                  <div class="row">
                     <div class="col-md-12" id="image-map-wrapper">
                        <div id="image-map-container">
                           <div id="image-map" style="max-width: 100%" class="image-mapper">
                              <img class="image-mapper-img">
                              <svg class="image-mapper-svg" style="width: 100%;"></svg>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <table class="table" id="image-mapper-table">
                  <thead>
                     <tr>
                        <th>Attivo</th>
                        <th>Forma</th>
                        <th>Link</th>
                        <th>Titolo</th>
                        <th>Target</th>
                        <th style="width: 25px"></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td style="width: 65px">
                           <div class="control-label input-sm"><input type="radio" name="im[0][active]" value="1" checked="checked"></div>
                        </td>
                        <td>
                           <select name="im[0][shape]" class="form-control input-sm">
                              <option value="">---</option>
                              <option value="rect">Rettangolo</option>
                              <option value="poly">Poligono</option>
                              <option value="circle">Cerchio</option>
                           </select>
                        </td>
                        <td><input type="text" name="im[0][href]" value="" placeholder="Link" class="form-control input-sm"></td>
                        <td><input type="text" name="im[0][title]" value="" placeholder="Titolo" class="form-control input-sm"></td>
                        <td>
                           <select name="im[0][target]" class="form-control input-sm">
                              <option value="">---</option>
                              <option value="_blank">_blank</option>
                              <option value="_parent">_parent</option>
                              <option value="_self">_self</option>
                              <option value="_top">_top</option>
                           </select>
                        </td>
                        <td>
                            <button class="btn btn-danger btn-sm remove-row" name="im[0][remove]" value="">Cancella</button></td>
                     </tr>
                  </tbody>
                  <tfoot>
                     <tr>
                        <th colspan="6" style="text-align: right"><button type="button" class="btn btn-danger btn-sm add-row"><span class="glyphicon glyphicon-plus"></span> Aggiungi una nuova area</button></th>
                     </tr>
                  </tfoot>
               </table>
            </div>
         </div>
      </div>
      <div class="container toggle-content segment" style="display: none;">
         <div class="row">
            <div class="col-md-12" style="text-align: center"><button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#modal-code">Mostra il codice della mappa</button></div>
         </div>
      </div>

      <div class="modal fade" id="image-mapper-load" tabindex="-1" role="dialog" aria-labelledby="image-mapper-load-label" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content" id="image-mapper-dialog">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title" id="image-mapper-load-label">Load Image from Website</h4>
               </div>
               <div class="modal-body">
                  <div class="input-group input-group-sm has-error"><input type="text" value="" placeholder="http://..." id="image-mapper-url" class="form-control input-sm"><span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span></div>
               </div>
               <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary" id="image-mapper-continue">Continue</button></div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="modal-code" tabindex="-1" role="dialog" aria-labelledby="modal-code-label" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content" id="modal-code-dialog">
                <form action="{{ Route('mapStore') }}" method="POST">
                    @csrf
                    <input name="imageUpload" type="hidden" id="image-upload">
                    <input name="imageName" type="hidden" id="image-name">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="modal-code-label"></h4>
                    </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Nome della mappa" name="name">
                    </div>
                    <div class="form-group">
                        <textarea name="map" class="form-control input-sm" readonly="readonly" id="modal-code-result" rows="10"></textarea>
                    <p class="muted">
                        Salva prima di testare la preview
                    </p>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
         </div>
      </div>
      <script src="/js/app.js"></script>
      <script>(function($){ $(document).trigger('init'); })(jQuery);</script>



   @endsection
